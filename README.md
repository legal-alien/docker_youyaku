実験的  (experimental)

# docker_youyaku

JUMAN KNP Shuca で日本語サマライズを実行できる Docker イメージ

Docker run して  `-it` でログインして使用。

## 使い方詳細

(ToDo)  ※ Docker イメージはできたが、デカイ・・・色々と検討中。

### リンク

* [日本語形態素解析システム JUMAN](http://nlp.ist.i.kyoto-u.ac.jp/index.php?JUMAN)
* [日本語構文・格・照応解析システム KNP](http://nlp.ist.i.kyoto-u.ac.jp/index.php?KNP)
* [GitHub - Shuca](https://github.com/hitoshin/shuca)
* [MOONGIFT -Shuca - 日本語対応の自動要約](https://www.moongift.jp/2015/09/shuca-%E6%97%A5%E6%9C%AC%E8%AA%9E%E5%AF%BE%E5%BF%9C%E3%81%AE%E8%87%AA%E5%8B%95%E8%A6%81%E7%B4%84/)
* [Shuca (朱夏) - 日本語対応のサマライザ](https://blog.bsdhack.org/index.cgi/Computer/20151001.htm)
* [自動要約プログラムshucaを使ってニ郎コピペを要約した](http://soy-curd.hatenablog.com/entry/2015/10/04/194722)
