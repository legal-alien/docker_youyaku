FROM python:2

WORKDIR /tmp

# TODO PreRquire
RUN mkdir /usr/local/dic/
RUN mkdir /usr/local/libexec/

# Shuca
RUN wget -q https://github.com/hitoshin/shuca/archive/master.zip
RUN unzip master.zip
RUN mkdir -p /usr/local/{bin,libexec,dic}
RUN cp -p shuca-master/lib/* /usr/local/bin/.
RUN cp -p shuca-master/dic/* /usr/local/dic/.
RUN cp -p shuca-master/libexec/* /usr/local/libexec/.

# JUMAN
RUN wget -q http://nlp.ist.i.kyoto-u.ac.jp/nl-resource/juman/juman-7.01.tar.bz2
RUN tar xvf juman-7.01.tar.bz2
RUN cd /tmp/juman-7.01 && \
    ./configure --prefix=/usr/local && \
    make && \
    make install

# KNP
RUN cd ../
RUN apt install zlib1g
RUN apt install zlib1g-dev
RUN wget -q http://nlp.ist.i.kyoto-u.ac.jp/nl-resource/knp/knp-4.19.tar.bz2
RUN tar xvf knp-4.19.tar.bz2
RUN cd knp-4.19 && \
    ./configure --prefix=/usr/local --with-juman-prefix=/usr/local && \
    make && \
    make install

# link
RUN ldconfig -v

CMD ["Shuca.py"]